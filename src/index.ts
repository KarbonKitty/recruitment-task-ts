export type ServiceYear = 2020 | 2021 | 2022;
export type ServiceType = "Photography" | "VideoRecording" | "BlurayPackage" | "TwoDayEvent" | "WeddingSession";

const prerequisites = {
    "BlurayPackage": [ "VideoRecording" ],
    "TwoDayEvent": [ "Photography", "VideoRecording" ]
};

const servicePrices = {
    2020: {
        "Photography": 1700,
        "VideoRecording": 1700,
        "WeddingSession": 600,
        "BlurayPackage": 300,
        "TwoDayEvent": 400 },
    2021: {
        "Photography": 1800,
        "VideoRecording": 1800,
        "WeddingSession": 600,
        "BlurayPackage": 300,
        "TwoDayEvent": 400 },
    2022: {
        "Photography": 1900,
        "VideoRecording": 1900,
        "WeddingSession": 600,
        "BlurayPackage": 300,
        "TwoDayEvent": 400 }
}

type ServicePackage = { services: ServiceType[], price: number };

const servicePackages: { [key: number]: ServicePackage[] } = {
    2020: [
        { services: [ "Photography", "VideoRecording", "WeddingSession" ], price: 2500 },
        { services: [ "Photography", "VideoRecording" ], price: 2200 },
        { services: [ "Photography", "WeddingSession" ], price: 2000 },
        { services: [ "VideoRecording", "WeddingSession" ], price: 2000 }
    ],
    2021: [
        { services: [ "Photography", "VideoRecording", "WeddingSession" ], price: 2600 },
        { services: [ "Photography", "VideoRecording" ], price: 2300 },
        { services: [ "Photography", "WeddingSession" ], price: 2100 },
        { services: [ "VideoRecording", "WeddingSession" ], price: 2100 }
    ],
    2022: [
        { services: [ "Photography", "VideoRecording", "WeddingSession" ], price: 2500 },
        { services: [ "Photography", "VideoRecording" ], price: 2500 },
        { services: [ "Photography", "WeddingSession" ], price: 1900 },
        { services: [ "VideoRecording", "WeddingSession" ], price: 2200 }
    ]
};

export const updateSelectedServices = (
    previouslySelectedServices: ServiceType[],
    action: { type: "Select" | "Deselect"; service: ServiceType }
) => {
    if (action.type === "Select") {
        return trySelectService(previouslySelectedServices, action.service);
    } else if (action.type === "Deselect") {
        return deselectService(previouslySelectedServices, action.service);
    }
};

export const calculatePrice = (selectedServices: ServiceType[], selectedYear: ServiceYear) => {
    let basePrice = selectedServices.map(s => servicePrices[selectedYear][s]).reduce((a, b) => a + b, 0);

    let finalPrice = 0;
    let unpricedServices = [...selectedServices];
    const servicePackagesForTheYear = servicePackages[selectedYear];
    servicePackagesForTheYear.forEach(servicePackage => {
        if (containsServicePackage(unpricedServices, servicePackage.services))
        {
            finalPrice += servicePackage.price;
            unpricedServices = removeServicePackage(unpricedServices, servicePackage.services);
        }
    });
    finalPrice = unpricedServices.map(s => servicePrices[selectedYear][s]).reduce((a, b) => a + b, finalPrice);

    return { basePrice, finalPrice };
};

const trySelectService = (currentServices: ServiceType[], newService: ServiceType) => {
    // don't add duplicates
    if (currentServices.includes(newService)) {
        return currentServices;
    }

    return removeServicesLackingPrerequisites([...currentServices, newService]);
};

const deselectService = (currentServices: ServiceType[], serviceToRemove: ServiceType) =>
    removeServicesLackingPrerequisites(currentServices.filter(s => s != serviceToRemove));

const hasPrerequisites = (service: ServiceType) => !!prerequisites[service];

const arePrerequisitesFulfilled = (service: ServiceType, selectedServices: ServiceType[]) =>
    selectedServices.some(s => prerequisites[service].includes(s));

const removeServicesLackingPrerequisites = (selectedServices: ServiceType[]) =>
    selectedServices.filter(service => hasPrerequisites(service) ? arePrerequisitesFulfilled(service, selectedServices) : true);

const containsServicePackage = (selectedServices: ServiceType[], servicePackage: ServiceType[]) =>
    servicePackage.every(s => selectedServices.includes(s));

const removeServicePackage = (selectedServices: ServiceType[], servicePackage: ServiceType[]) =>
    selectedServices.filter(s => !servicePackage.includes(s));
